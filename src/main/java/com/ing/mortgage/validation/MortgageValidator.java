package com.ing.mortgage.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ing.mortgage.annotation.MortgageFeasibilityValidator;
import com.ing.mortgage.model.MortgageDetails;
import com.ing.mortgage.service.MortgageService;
import com.ing.mortgage.service.MortgageServiceImpl;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class MortgageValidator implements ConstraintValidator<MortgageFeasibilityValidator, MortgageDetails> {

	private static final Logger log = LoggerFactory.getLogger(MortgageServiceImpl.class);
	
	@Autowired
	private MortgageService mortgageService; 
	
	@Override
	public boolean isValid(MortgageDetails mortgageDetails, ConstraintValidatorContext context) {
		log.info("Calling service class for validating mortgage feasibility with the details : " + mortgageDetails.toString());
		return mortgageService.checkMortgageFeasibility(mortgageDetails);
	}
	
}