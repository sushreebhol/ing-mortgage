package com.ing.mortgage.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.ing.mortgage.validation.MortgageValidator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Documented
@Constraint(validatedBy = MortgageValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)

public @interface MortgageFeasibilityValidator {
	
	String message() default "Validation Failed. Loan value should not exceed 4 times the annual income and the loan value should not exceed the home value";
	
    Class<?>[] groups() default {};
    
    Class<? extends Payload>[] payload() default {};
    
    String loanValue();
}
