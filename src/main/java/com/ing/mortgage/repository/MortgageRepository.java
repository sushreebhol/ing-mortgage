package com.ing.mortgage.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ing.mortgage.entities.MortgageRate;

public interface MortgageRepository extends JpaRepository<MortgageRate, Integer> {
	
	@Query("SELECT mr.interestRate from MortgageRate mr where mr.maturityPeriod = :maturityPeriod")
	public float findInterestRatebyMaturityPeriod(int maturityPeriod);
}
