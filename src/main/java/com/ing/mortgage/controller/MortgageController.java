package com.ing.mortgage.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ing.mortgage.model.InterestRates;
import com.ing.mortgage.model.MonthlyMortgageCost;
import com.ing.mortgage.model.MortgageDetails;
import com.ing.mortgage.service.MortgageService;

import jakarta.validation.Valid;

@RestController
public class MortgageController {

	private static final Logger log = LoggerFactory.getLogger(MortgageController.class);
	
	@Autowired
	private MortgageService mortgageService;
	
	@GetMapping("/api/interestRates")
	public List<InterestRates> getInterestRates() {
		log.info("Getting all the current interest rates with the maturity period..");
		return mortgageService.getCurrentInterestRates();
	}
	
	@GetMapping("/api/mortgageCheck")
	public MonthlyMortgageCost getMortgageDetails(@Valid @RequestBody MortgageDetails mortgageDetails) {
		log.info("Getting the monthly mortgage cost with the details :" + mortgageDetails.toString());
		return mortgageService.getMonthlyMortgageCost(mortgageDetails);
	}
}
