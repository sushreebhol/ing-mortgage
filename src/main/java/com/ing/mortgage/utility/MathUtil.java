package com.ing.mortgage.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ing.mortgage.model.MortgageDetails;
import com.ing.mortgage.service.MortgageServiceImpl;

public class MathUtil {
	
	private static final Logger log = LoggerFactory.getLogger(MortgageServiceImpl.class);
	
	public static float calculateMonthlyMortgageCost(MortgageDetails mortgageDetails, float interestRate) {
		
		float mortgageCost = mortgageDetails.getLoanValue();
		int totalPeriodInMonths = (mortgageDetails.getMaturityPeriod()) * 12;
		
		log.info("Calculating monthly mortgage cost with interest rate : " + interestRate + 
				 " Total Mortgage Cost : " + mortgageCost + " Maturity period : " + totalPeriodInMonths + " months");
		
		float monthlyMortgageCost = (((mortgageCost * interestRate) / 100) + mortgageCost) / totalPeriodInMonths;
		
		return monthlyMortgageCost;
	}
}
