package com.ing.mortgage.model;

public class MonthlyMortgageCost {
	
	private boolean mortgageFeasiblity;
	private String monthlyMortgageCost;
	
	public boolean isMortgageFeasiblity() {
		return mortgageFeasiblity;
	}
	public void setMortgageFeasiblity(boolean mortgageFeasiblity) {
		this.mortgageFeasiblity = mortgageFeasiblity;
	}
	public String getMonthlyMortgageCost() {
		return monthlyMortgageCost;
	}
	public void setMonthlyMortgageCost(String monthlyMortgageCost) {
		this.monthlyMortgageCost = monthlyMortgageCost;
	}

}
