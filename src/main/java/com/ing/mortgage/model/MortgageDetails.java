package com.ing.mortgage.model;

import com.ing.mortgage.annotation.MortgageFeasibilityValidator;

import jakarta.validation.constraints.Max;

@MortgageFeasibilityValidator(loanValue = "loanValue")
public class MortgageDetails {
	
	@Max(value = 20)
	private int maturityPeriod;
	
	private float annualIncome;
	
	private float loanValue;
	
	private float homeValue;

	private boolean mortgageFeasibility;
	
	public int getMaturityPeriod() {
		return maturityPeriod;
	}

	public void setMaturityPeriod(int maturityPeriod) {
		this.maturityPeriod = maturityPeriod;
	}

	public float getAnnualIncome() {
		return annualIncome;
	}

	public void setAnnualIncome(float annualIncome) {
		this.annualIncome = annualIncome;
	}

	public float getLoanValue() {
		return loanValue;
	}

	public void setLoanValue(float loanValue) {
		this.loanValue = loanValue;
	}

	public float getHomeValue() {
		return homeValue;
	}

	public void setHomeValue(float homeValue) {
		this.homeValue = homeValue;
	}

	public boolean isMortgageFeasibility() {
		return mortgageFeasibility;
	}

	public void setMortgageFeasibility(boolean mortgageFeasibility) {
		this.mortgageFeasibility = mortgageFeasibility;
	}

	@Override
	public String toString() {
		return "MortgageDetails [maturityPeriod=" + maturityPeriod + ", annualIncome=" + annualIncome + ", loanValue="
				+ loanValue + ", homeValue=" + homeValue + "]";
	}
}
