package com.ing.mortgage.model;

public class InterestRates {
	
	private int maturityPeriod;
	
	private String interestRate;

	public InterestRates(int maturityPeriod, String interestRate) {
		super();
		this.maturityPeriod = maturityPeriod;
		this.interestRate = interestRate;
	}

	public int getMaturityPeriod() {
		return maturityPeriod;
	}

	public void setMaturityPeriod(int maturityPeriod) {
		this.maturityPeriod = maturityPeriod;
	}

	public String getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(String interestRate) {
		this.interestRate = interestRate;
	}
	
}
