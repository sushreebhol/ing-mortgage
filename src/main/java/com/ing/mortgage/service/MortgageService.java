package com.ing.mortgage.service;

import java.util.List;

import com.ing.mortgage.model.InterestRates;
import com.ing.mortgage.model.MonthlyMortgageCost;
import com.ing.mortgage.model.MortgageDetails;

public interface MortgageService {

	public List<InterestRates> getCurrentInterestRates();
	
	public MonthlyMortgageCost getMonthlyMortgageCost(MortgageDetails mortgageDetails);
	
	public boolean checkMortgageFeasibility(MortgageDetails mortgageDetails);
}
