package com.ing.mortgage.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ing.mortgage.entities.MortgageRate;
import com.ing.mortgage.model.InterestRates;
import com.ing.mortgage.model.MonthlyMortgageCost;
import com.ing.mortgage.model.MortgageDetails;
import com.ing.mortgage.repository.MortgageRepository;
import com.ing.mortgage.utility.MathUtil;


@Service
public class MortgageServiceImpl implements MortgageService {

	private static final Logger log = LoggerFactory.getLogger(MortgageServiceImpl.class);
	
	@Autowired
	private MortgageRepository mortgageRepository;

	public List<InterestRates> getCurrentInterestRates() {
		log.info("Getting all interst rates with the maturity period..");
		List<MortgageRate> mortgageRatesList = mortgageRepository.findAll();

		return mortgageRatesList.stream()
				.map(mortgageRate -> new InterestRates(mortgageRate.getMaturityPeriod(),(float) mortgageRate.getInterestRate() + "%"))
				.collect(Collectors.toList());
	}

	public MonthlyMortgageCost getMonthlyMortgageCost(MortgageDetails mortgageDetails) {
		float interestRate = getInterestRateByMaturityPeriod(mortgageDetails.getMaturityPeriod());
		
		log.info("Calculating monthly mortgage cost..");
		float monthlyMortgageAmount = MathUtil.calculateMonthlyMortgageCost(mortgageDetails, interestRate);
		
		MonthlyMortgageCost monthlyMortgageCost = new MonthlyMortgageCost();
		monthlyMortgageCost.setMonthlyMortgageCost(monthlyMortgageAmount + " euro");
		monthlyMortgageCost.setMortgageFeasiblity(mortgageDetails.isMortgageFeasibility());
		
		return monthlyMortgageCost;
	}

	public float getInterestRateByMaturityPeriod(int maturityPeriod) {
		log.info("Calling repository to get the interest rate for maturity period " + maturityPeriod);
		return mortgageRepository.findInterestRatebyMaturityPeriod(maturityPeriod);
	}

	public boolean checkMortgageFeasibility(MortgageDetails mortgageDetails) {
		float annualIncome = mortgageDetails.getAnnualIncome();
		float loanValue = mortgageDetails.getLoanValue();
		float homeValue = mortgageDetails.getHomeValue();
		boolean isFeasibleForLoan = true;
		
		log.info("Checking if the mortgage is feasible or not with annual income : " + annualIncome + 
				 " loan value : " + loanValue + " home value : " + homeValue);
		
		if (loanValue > (annualIncome * 4)) {
			isFeasibleForLoan = false;
		} else if (loanValue > homeValue) {
			isFeasibleForLoan = false;
		} 
		mortgageDetails.setMortgageFeasibility(isFeasibleForLoan);
		return isFeasibleForLoan;
	}
}
